A C program that encrypts plaintext to ciphertext and can also decrypt back to plaintext. The program also encodes and decodes plaintext to/from binary.
How to use:
For encrypting plaintext, type encrypt(your plaintext here) 
For decrypting plaintext, type decrypt(your ciphertext here)
For encoding to  binary, type encode(plaintext)
For decoding binary, type decode(10001000)