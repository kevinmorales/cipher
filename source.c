#include <stdio.h>
#include <string.h>
#include <math.h>

int validity(char*);
int convert(char[]);
/***************************
This is the Cipher program for CSC 322.
Users can encrypt plaintext, decrypt plaintext,
encode plaintext, and decode 8-bit binary.
****************************/

int main(){
    //local constants
    const char* EXIT = "exit";      //Sentinel value to end program
    const char* ENCRYPT = "encrypt";//Command to encrypt
    const char* DECRYPT = "decrypt";//Command to decrypt
    const char* ENCODE = "encode";  //Command to encode
    const char* DECODE = "decode";  //Command to decode
    const unsigned int SHIFT = 5;   //Cipher shift value
    const unsigned int LOOP = 95;   //ASCII Loop value
    const unsigned int MAX = 126;   //Maximum ASCII value
    const unsigned int MIN = 33;    //Minimum ASCII value

    //local variables
    char input[50];                 //Array for user input
    char* token;                    //Pointer for parsing input
    char substring[9];              //Array for obtaining substring
    char result[10];                //Array for decoded binary
    int binary[8] = {0,0,0,0,0,0,0,0,};//Array for binary
    int x,y;                          //Multi-use integer variables
    /***********************/

    //Get input from the user
    printf("Enter command: ");
    fgets(input,50,stdin);

    //WHILE the user does not want to exit
    while(strncmp(input, EXIT,4) != 0){

        //Get the first token
        token = strtok(input,"(");

        //IF the user wants to encrypt
        if(strcmp(token, ENCRYPT) == 0){
            //Get the text to encrypt
            token = strtok(NULL, ")");
                    //FOR each character in the token
                for(int i = 0; i < strlen(token);i++){
                    //IF the character's ASCII value plus the shift is greater than the MAX ASCII value
                    if(token[i] + SHIFT > MAX){
                        //Subtract the LOOP value to simulate a loop back to the MIN ASCII value
                        token[i] -= LOOP;
                    }//END IF
                    //Increase the character's ASCII value by the SHIFT value
                    token[i] += SHIFT;
                }//END FOR
                //Print the resulting encryption
                printf("%s", token);
        }
        //ELSE IF user wants to decrpyt
        else if(strcmp(token, DECRYPT) == 0){
            //Get the text to decrpyt
            token = strtok(NULL, ")");
            printf("%s\n", token);
                //FOR each character in the token
                for(int i = 0; i < strlen(token);i++){
                    //IF the character's minus the shift is less than the MIN ASCII value
                    if(token[i] - SHIFT < MIN){
                        //ADD the LOOP value to simulate a loop to the MAX ASCII value
                        token[i] += LOOP;
                    }//END IF
                    //Lowet the character's ASCII value by the SHIFT value
                    token[i] -= SHIFT;
                }//END FOR
                //Print the resulting decrpytion
                printf("%s", token);
        }
        //ELSE IF the user wants to encode to binary
        else if(strcmp(token, ENCODE) == 0){
            //Get the next token
            token = strtok(NULL, ")");
            printf("%s\n", token);

            //IF the user did not enter any characters
            if(strlen(token) == 0){
                //Print the error message
                printf("[Error] Please enter at least one character!\n");
            }
            //ELSE the user entered valid characters
            else{
                //FOR each character in the token
                for(int j = 0;j < strlen(token); j++){
                    //Initialize index variable to 7
                    y = 7;
                    //Store the value of the character in a placeholder variable
                    x = token[j];
                    //WHILE the character's ASCII value is greater than 0
                    while(x > 0){
                        //Store the result of character value mod 2 in the current array index
                        binary[y] = x % 2;
                        //Decrement the index value
                        y--;
                        //Divide the character's value by 2
                        x = x / 2;
                    }//END WHILE
                    //Print out the binary array
                    for(int i = 0; i < 8;i++){
                        printf("%d", binary[i]);
                        //Reset the value of the array index to 0
                        binary[i] = 0;
                    }//END FOR
                }//END FOR
            }//END IF
        }
        //ELSE IF the user wants to decode binary
        else if(strcmp(token, DECODE) == 0){
            //Get the token
            token = strtok(NULL,")");
            //IF the token length is not divisible by 8
            if( strlen(token) % 8 != 0){
                printf("[Error]This will only decode 8-bit binary\n");
            }
            //ELSE IF the token is not purely 1's and 0's
            else if(validity(token) != 0){
                printf("[ERROR]Please enter only binary characters 1 and 0\n");
            }
            //ELSE IF the token is not 8-bit binary
            else if(strlen(token) < 8){
                printf("[ERROR} Please enter 8 bit binary\n");
            }
            //ELSE the user entered valid binary
            else{
                //Initialize x to 0
                x = 0;
                //WHILE x is less than the length of the token
                while(x < strlen(token)){
                    //Create a character array from the token pointer
                    for(int i = 0; i < 8; i++){
                        substring[i] = token[x];
                        x++;
                    }//END FOR
                    substring[8] = '\0';
                    //Call function to convert from binary to base-10 integer
                    y = convert(substring);
                    //Print out the integer as a ASCII character
                    printf("%c", y);
                }//END WHILE
            }//END IF
        }
        ///ELSE the user entered an invalid command
        else{
            printf("\nPlease enter the command correctly.\n");
        }//END IF
        //Prompt the user for another command
        printf("\nEnter command: ");
        fgets(input, 50, stdin);
    }//END WHILE
    //Thank user for using program
    printf("Thanks for using my Cipher program!\n");
    //RETURN 0
    return 0;
}//END main

/*********************
This function converts 8-bit binary to a base-10 integer
This function takes a character pointer as a parameter
and returns an integer as a return value.
**********************/
int convert(char * a){
    //local constants
    //local variables
    int counter = 7;    //Initialize power value to 7
    int x = 0;          //Initialize base-10 integer to 0
    /**************/
    //FOR each character in the string
    for(int i = 0; i < strlen(a);i++){
        //IF the character is a 1
        if(a[i] == 49){
            //Increment the integer by 2 raised to the power of the counter
            x += pow(2,counter);
        }//END IF
        //Decrement the power value
        counter--;
    }//END FOR
    //RETURN the integer
    return x;

}//END convert
/*******************
This function checks if all characters in the user
input are 0's and 1's. This is to ensure that the user
only entered proper binary. This function takes a
character array as input and returns a zero if everything
checks out well and a 1 if there is at lease one
invalid character.
********************/
int validity(char a[]){
    //local constants
    //local variables
    int x = 0;      //Initialize the return value to 0
    /****************/
    //FOR each character in the string
    for(int i = 0; i< strlen(a); i++){
        //IF the character is not a zero or one
        if(a[i] > 49 || a[i] < 48){
            //Set the return value to 1
            x = 1;
            //Break out of the for loop
            break;
        }//END IF
    }//END FOR
    //RETURN the value of x
    return x;
}//END validity



